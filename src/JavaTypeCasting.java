public class JavaTypeCasting {
    public class Main {
        public static void main(String[] args) {
            int myInt = 9;
            double myDouble = myInt; // Automatic casing: int to double

            System.out.println(myInt);
            System.out.println(myDouble);
        }
    } 
}
