public class JavaTypeCasting2 {
    public class Main {
        public static void main(String[] args) {
            double myDouble1 = 9.78d;
            int myInt1 = (int) myDouble1; // Manual casting: double to int

            System.out.println(myDouble1); 
            System.out.println(myInt1);
        }
    }
}
